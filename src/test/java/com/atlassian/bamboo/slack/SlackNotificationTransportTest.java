package com.atlassian.bamboo.slack;

import com.atlassian.bamboo.notification.buildhung.BuildHungNotification;
import com.atlassian.bamboo.plan.Plan;
import com.atlassian.bamboo.plan.PlanKeys;
import com.atlassian.bamboo.plan.PlanResultKey;
import com.atlassian.bamboo.project.Project;
import com.atlassian.bamboo.variable.CustomVariableContext;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.lang.reflect.Field;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;

@RunWith (MockitoJUnitRunner.class)
public class SlackNotificationTransportTest
{
    private final String WEBHOOK_URL = "alamakotaakotmaapitoken";
    private final String CHANNEL = "#theRoom";
    private final String ICON_URL = "http://upload.wikimedia.org/wikipedia/commons/thumb/4/40/Wave.svg/170px-Wave.svg.png";

    @Mock
    private Project project;
    @Mock
    private Plan plan;
    @Mock
    private CustomVariableContext customVariableContext;

    @Test
    public void testCorrectUrlsAreHit()
    {
        when(project.getKey()).thenReturn("BAM");
        when(plan.getProject()).thenReturn(project);
        when(plan.getBuildKey()).thenReturn("MAIN");
        when(plan.getName()).thenReturn("Main");
        when(customVariableContext.substituteString("alamakotaakotma")).thenReturn("alamakotaakotma");
        when(customVariableContext.substituteString("alamakotaakotmaapitoken")).thenReturn("alamakotaakotmaapitoken");
        when(customVariableContext.substituteString("#theRoom")).thenReturn("#theRoom");
        when(customVariableContext.substituteString(ICON_URL)).thenReturn(ICON_URL);


        final PlanResultKey planResultKey = PlanKeys.getPlanResultKey("BAM-MAIN", 3);

        BuildHungNotification notification = new BuildHungNotification()
        {
            public String getHtmlImContent()
            {
                return "IM Content";
            }

        };

        SlackNotificationTransport hnt = new SlackNotificationTransport(WEBHOOK_URL, CHANNEL, ICON_URL, plan, null, null, customVariableContext);

        //dirty reflections trick to inject mock HttpClient
        try
        {
            Field field = SlackNotificationTransport.class.getDeclaredField("client");
            field.setAccessible(true);
            field.set(hnt, new MockHttpClient());
        }
        catch (Exception e)
        {
            e.printStackTrace();
            fail(e.getMessage());
        }

        hnt.sendNotification(notification);
    }

    public class MockHttpClient extends org.apache.commons.httpclient.HttpClient
    {
        public int executeMethod(HttpMethod method)
                throws IOException
        {
            assertTrue(method instanceof PostMethod);
            PostMethod postMethod = (PostMethod) method;
            assertEquals(WEBHOOK_URL, method.getURI().toString());

            try {
                JSONObject payload = new JSONObject(postMethod.getParameter("payload").getValue());

                assertEquals(CHANNEL, payload.getString("channel"));
                assertEquals("Bamboo", payload.getString("username"));
                assertEquals(ICON_URL, payload.get("icon_url"));

            } catch (JSONException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }

            return 0;
        }
    }
}
