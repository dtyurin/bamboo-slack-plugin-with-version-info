package com.atlassian.bamboo.slack;


import junit.framework.Assert;
import org.junit.Test;

public class SlackMessageParsingTest {

    private final String INPUT = "<img src='http://SOP-MARACHE.local:6990/bamboo/images/iconsv4/icon-build-successful.png' height='16' width='16' align='absmiddle' />&nbsp;<a href='http://SOP-MARACHE.local:6990/bamboo/browse/TEST-TEST-8'>test &rsaquo; test &rsaquo; #8</a> passed. Manual run by <a href=\"http://SOP-MARACHE.local:6990/bamboo/browse/user/admin\">admin</a>";
    private final String OUTPUT_FALLBACK = "test › test › #8 passed. Manual run by admin";
    private final String OUTPUT_TEXT = "<http://SOP-MARACHE.local:6990/bamboo/browse/TEST-TEST-8|test › test › #8> passed. Manual run by <http://SOP-MARACHE.local:6990/bamboo/browse/user/admin|admin>";

    private final String BRANCH_INPUT = "<img src='http://sop-marache.cstb.local:6990/bamboo/images/iconsv4/icon-build-failed.png' height='16' width='16' align='absmiddle' />&nbsp;<a href='http://sop-marache.cstb.local:6990/bamboo/browse/AA-SLAP0-5'>Atlassian Anarchy &rsaquo; Sounds like a plan &rsaquo; <img src='http://sop-marache.cstb.local:6990/bamboo/images/icons/branch.png' height='16' width='16' align='absmiddle' />&nbsp;test-branch &rsaquo; #5</a> failed. Manual run by <a href=\"http://sop-marache.cstb.local:6990/bamboo/browse/user/admin\">Admin</a>\n";
    private final String BRANCH_OUTPUT_FALLBACK = "Atlassian Anarchy › Sounds like a plan › test-branch › #5 failed. Manual run by Admin";
    private final String BRANCH_OUTPUT_TEXT = "<http://sop-marache.cstb.local:6990/bamboo/browse/AA-SLAP0-5|Atlassian Anarchy › Sounds like a plan › test-branch › #5> failed. Manual run by <http://sop-marache.cstb.local:6990/bamboo/browse/user/admin|Admin>";

    @Test
    public void testFallbackMessage()
    {
        String branch_output = SlackNotificationTransport.fallbackMessage(BRANCH_INPUT);
        String output = SlackNotificationTransport.fallbackMessage(INPUT);
        Assert.assertEquals(OUTPUT_FALLBACK,output);
        Assert.assertEquals(BRANCH_OUTPUT_FALLBACK,branch_output);
    }

    @Test
    public void testTextMessage()
    {
        String output = SlackNotificationTransport.textMessage(INPUT);
        String branch_output = SlackNotificationTransport.textMessage(BRANCH_INPUT);
        Assert.assertEquals(OUTPUT_TEXT,output);
        Assert.assertEquals(BRANCH_OUTPUT_TEXT,branch_output);
    }
}
