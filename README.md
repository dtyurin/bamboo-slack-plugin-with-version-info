![Slack + Bamboo](http://cdn.appstorm.net/web.appstorm.net/web/files/2013/10/slack_icon.png)

Bamboo Slack Plugin
=====================
This plugin integrates [Bamboo](http://www.atlassian.com/software/bamboo) with [Slack](http://www.slack.com).

You can get it [here](https://marketplace.atlassian.com/plugins/com.atlassian.bamboo.plugins.bamboo-slack)

It allows any Bamboo notification to be posted to a specific slack room.

This plugin allows you to take advantage of Bamboo's:

* flexible notification system (ie tell me when this build fails more than 5 times!)
* commenting (comments show up in the chatroom so everyone can see someone's working on the build)

Notifications Supported
-----------------------
* Build successful
* Build failed
* Build commented
* Job hung
* Job queue timeout

Setup
-----
1. Go to the *Notifications* tab of the *Configure Plan* screen.
1. Choose a *Recipient Type* of *Slack*
1. Configure your *Slack domain* as in http://*domain*.slack.com
1. Configure your *Slack API token*, *Room Name* (to post message to) like #general.
1. You're done! Go and get building.

Feedback? Questions?
--------------------
Twitter: @0x4d4d or mathieu.marache@cstb.fr.